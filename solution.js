class Person
{
    constructor(name,age,salary,sex)
    {
      this.name = name;
      this.age = age;
      this.salary = salary;
      this.sex = sex;
    }

    static quicksort(personArray,field,order) 
    {
        let temporaryArray=[].concat(personArray);

        if (temporaryArray.length <= 1) 
        {
            return temporaryArray;
        }

          let pivot = temporaryArray[0][field];
          let pivotItems=temporaryArray[0];

          let left = []; 
          let right = [];

          for (let i = 1; i < temporaryArray.length; i++) {
              if(order==='asc')
              {temporaryArray[i][field] < pivot ? left.push(temporaryArray[i]) : right.push(temporaryArray[i]);}
              else
              {temporaryArray[i][field] < pivot ? right.push(temporaryArray[i]) : left.push(temporaryArray[i]);}
          }

          return Person.quicksort(left,field,order).concat(pivotItems, Person.quicksort(right,field,order));
    }
}

let person1=new Person('Ravi',11,50000,'M');
let person2=new Person('Rajesh',12,40000,'M');
let person3=new Person('Raju',13,30000,'M');
let person4=new Person('Rani',14,20000,'F');
let person5=new Person('Rekha',15,10000,'F');
const personArray=[person1,person2,person3,person4,person5];

console.log(Person.quicksort(personArray,'salary','asc'));